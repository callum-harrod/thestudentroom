<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Element Definition: "Slick Slider New"
 */

class PWL_Slick_Slider_New {

	public function ui() {
		return array(
			'title'       => __( 'Slick Slider', 'pw-cornerstone-extended' ),
		);
	}

	/**
	 * function: flags()
	 * The 'dynamic_child' allows child elements to render individually, but may cause
	 * styling or behavioral issues in the page builder depending on how your * shortcodes work. 
	 * If you have trouble with element presentation, try removing this flag.
	 */
	// public function flags() {

	// 	return array(
	// 		'dynamic_child' => true
	// 	);

	// }

	public function update_build_shortcode_atts( $atts ) {
		$atts['items'] = count( $atts['elements'] );
		return $atts;
	}
}

