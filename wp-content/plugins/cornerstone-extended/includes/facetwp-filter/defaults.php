<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Defaults Values: "FacetWP Filters"
 */

return array(

	'id'				=> '',
	'class'				=> '',
	'style'				=> '',
	'fwp_facet'			=> 'tax/category|categories',
	'show_filter_label'	=> '',
	'fwp_filter_label'	=> '',
	'fwp_label_position'	=> 'left',
	'fwp_type'			=> '',
	'button_color'		=> '',
	'show_reset_button'	=> '',
	'reset_button_text'	=> '',
	// 'show_sort_options'	=> '',
	
	// 'use_template'		=> '',
	// 'fwp_template'		=> '',
	
);

