<?php

/*
Plugin Name: Cornerstone Archive Module
Plugin URI: cornerstone-archive
Description: Adds dynamic archive module for Custom Post Types
Author: Pragmatic
Author URI: https://pragmatic.agency
Version: 1.0.0
*/

define( 'CORNERSTONE_ARCHIVE_PATH', plugin_dir_path( __FILE__ ) );
define( 'CORNERSTONE_ARCHIVE_URL', plugin_dir_url( __FILE__ ) );

add_action( 'wp_enqueue_scripts', 'my_extension_enqueue' );
add_action( 'cornerstone_register_elements', 'my_extension_register_elements' );
add_filter( 'cornerstone_icon_map', 'my_extension_icon_map' );

function my_extension_register_elements() {
	cornerstone_register_element( 'Cornerstone_Archive', 'cornerstone-archive', CORNERSTONE_ARCHIVE_PATH . 'includes/cornerstone-archive' );
	cornerstone_register_element( 'Cornerstone_Archive_New', 'cornerstone-archive-new', CORNERSTONE_ARCHIVE_PATH . 'includes/cornerstone-archive-new' );
}

function my_extension_enqueue() {
	wp_enqueue_style( 'my_extension-styles', CORNERSTONE_ARCHIVE_URL . '/assets/styles/my-extension.css', array(), '0.1.0' );
}

function my_extension_icon_map( $icon_map ) {
	$icon_map['cornerstone-archive'] = CORNERSTONE_ARCHIVE_URL . '/assets/svg/icons.svg';
	return $icon_map;
}


function cornerstone_archive_pagination( $current_page, $total_pages, $search_query = '' ){
	
	// Assuming we have more than a single page.
	if( $total_pages > 1 ) :

		// Case as integers, in case they've been pulled through as something else.
		settype( $current_page, "integer" );
		settype( $total_pages, "integer" );

		$_pagination_output .= '<section class="x-column x-sm x-1-1"><div class="cornerstone-archive-pagination">';

		// Start pagination count at 1.
		$pagination_count = 1;

		// While total number of pages less than our pagination loop.
		while( $total_pages >= $pagination_count ) :

			// If this is the current page - just output a span, not a link.
			if( $pagination_count === $current_page ) :

				$_pagination_output .= '<span class="pagination_link">' . $pagination_count . '</span>';

			else :

				// Set the page query.
				$archive_query = 'archive_page=' . $pagination_count;

				// If we have an active search query, append this to the query items.
				if( ! empty( $search_query ) ) :
					$archive_query .= '&archive_query=' . $search_query;
				endif;

				// Output a link with relevant query items and title tags.
				// $_pagination_output .= '<a href="?' . $archive_query . '" class="pagination_link" title="Go to page: ' . $pagination_count . '">' . $pagination_count . '</a>';

			endif;

			$pagination_count++;

		endwhile;

		// If currently viewing search query, output clear search button.
		if( ! empty ( $search_query ) ) :

			// $_pagination_output .= '<a href="?archive_query_clear=true" class="pagination_link" title="Clear search">Clear Search</a>';

		endif;

		$_pagination_output .= '</div></section>';

	else :

		// If there's only one page, but also a search query - show Clear search button.
		if( ! empty ( $search_query ) ) :

			$_pagination_output .= '<section class="x-column x-sm x-1-1"><div class="cornerstone-archive-pagination">';

			// $_pagination_output .= '<a href="?archive_query_clear=true" class="pagination_link" title="Clear search">Clear Search</a>';

			$_pagination_output .= '</div></section>';

		else :

			// For sanity - if single page only, return false.
			$_pagination_output = false;

		endif;

	endif;

	return $_pagination_output;

}
