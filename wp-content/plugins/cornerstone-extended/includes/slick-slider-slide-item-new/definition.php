<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Element Definition: "Slick Slider Slide Item"
 */

class PWL_Slick_Slider_Slide_Item_New {

	public function ui() {
		return array(
			'title'       => __( 'Slide Item', 'pw-cornerstone-extended' ),
		);
	}

	public function flags() {
		return array(
			'child' => true,
		);
	}

	public function update_build_shortcode_atts( $atts, $parent ) {

		if ( !isset( $atts['slider_height'] ) ) {
            $atts['slider_height'] = '';
        }

		if ( ! is_null( $parent ) ) {
			$atts['slider_height'] = $parent['slider_height'];
		}
		
		return $atts;

	}
}


