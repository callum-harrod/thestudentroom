<?php

/**
 * Creates the initial content for a newly-installed site.
 *
 * Adds sensible defaults for a new install
 *
 * @param int $user_id User ID.
 */

function wp_install_defaults( $user_id ) {

    /** Blog Description: "" **/
    update_option( 'blogdescription', '' );

    /** Mailserver: Clear content **/
    update_option( 'mailserver_url', '' );
    update_option( 'mailserver_login', '' );
    update_option( 'mailserver_pass', '' );
    update_option( 'mailserver_port', '' );

    /** Date format: "23rd November, 2014" */
    update_option( 'date_format', 'jS F, Y' );

    /** Time zone: "London" */
    update_option( 'timezone_string', 'Europe/London' );

    /** Permalink Structure **/
    update_option( 'permalink_structure', '/%postname%/' );

    /** Ping sites **/
    update_option( 'ping_sites', '' );

    /** Don't show the Welcome Panel **/
    update_user_meta( $user_id, 'show_welcome_panel', 0 );

    /** Set the default image link to none **/
    update_option( 'image_default_link_type', 'none' );

}
