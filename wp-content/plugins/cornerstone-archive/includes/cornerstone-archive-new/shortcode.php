<?php

/**
 * Shortcode handler
 */

	$_archive_query_var = $_GET['archive_query'];

	if ($_archive_query_var == '') {
		$_archive_query_var = 'Search';
	}

	$_content = '';
	$_content .= '<div class="x-search-shortcode cornerstone-archive-search search-page">';
	$_content .= '<form method="get" id="searchform" class="form-search">';
	$_content .= '<div class="search-query-container"><input type="text" id="archive_query" class="search-query" placeholder="' . $_archive_query_var . '" name="archive_query"><input type="submit" id="searchsubmit" value="Search"></div>';
	$_content .= '</form></div>';

	$_paged_query_var = $_GET['archive_page'];

	if( ! $_paged_query_var || ! is_numeric( $_paged_query_var ) ) :
		$_paged_query_var = 1;
	endif;

	$_search_query_var = $_GET['archive_query'];

	$available_post_types = get_post_types(
		array(
			'public' => true,
		)
	);

	if ( false !== ( $key = array_search($post_type, $available_post_types) ) ) {
    	unset( $available_post_types[$key] );
	}

	if( ! $_search_query_var ) :

		$_search_query = '';

		// If no search query set, do first query on Post Type
		$_args = array(
			'post_type'			=> $post_type,
			'posts_per_page'	=> -1,
			'paged'				=> $_paged_query_var,
			'order'				=> 'ASC',
			'orderby'			=> 'menu_order title',
			'meta_query' => array(
				array(
					'key'     => '_thumbnail_id',
				),
			),
		);

		$_resources = new WP_Query( $_args );

		// If no search query set, do second query on every other Post Type
		$_args_explore = array(
			'post_type'			=> $available_post_types,   // Get all public post types, and exclude $post_type from array
			'posts_per_page'	=> $count,
			'paged'				=> $_paged_query_var,
			'order'				=> 'ASC',
			'orderby'			=> 'menu_order title',
			'meta_query' => array(
				array(
					'key'     => '_thumbnail_id',
				),
			),
		);

		$_resources_explore = new WP_Query( $_args_explore );

	else :

		$_search_query = esc_attr( $_search_query_var );

		// If search query IS set, do second query on Post Type using SearchWP
		$_args = array(
			'post_type'			=> $post_type,
			'posts_per_page'	=> -1,
			'page'				=> $_paged_query_var,
			's'					=> $_search_query,
			'meta_query' => array(
				array(
					'key'     => '_thumbnail_id',
				),
			),
		);

		$_search_resources = new SWP_Query( $_args );

		// If search query IS set, do second query on every other Post Type with search term using SearchWP
		$_args_explore = array(
			'post_type'			=> $available_post_types,   // Get all public post types, and exclude $post_type from array
			'posts_per_page'	=> $count,
			'page'				=> $_paged_query_var,
			's'					=> $_search_query,
			'engine' 			=> 'explore',
			'meta_query' => array(
				array(
					'key'     => '_thumbnail_id',
				),
			),
		);

		$_search_resources_explore = new SWP_Query( $_args_explore );

	endif;

	// Tab Menu

	if ( $_search_query_var != '' ) {
		$_content .= '<div class="clear-search-container"><a href="?archive_query_clear=true" class="pagination_link clear-search" title="Clear search">Clear Search</a></div>';
	}

	$_content .= '<ul class="tab-menu">';
		$_content .= '<li class="tab-activator"><button class="active" data-tab-id="tab-1"> Products </button></li>';
		$_content .= '<li class="tab-activator"><button class="" data-tab-id="tab-2"> Explore </button></li>';
	$_content .= '</ul>';

	// ------------------------------------------------------------------------------------------------------------------------------
	// -----------------------------  Start of the first tab for the search page ----------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------


	if( ( $_resources && $_resources->have_posts() ) || ( $_search_resources && ( ! empty( $_search_resources->posts ) ) ) ) :

		$i = 0;

		
		$_content .= '<div class="x-container tab active" id="tab-1">';

		if( $_resources ) :
			$_number_of_pages = $_resources->max_num_pages;
		elseif( $_search_resources ) :
			$_number_of_pages = $_search_resources->max_num_pages;
		endif;

		if( $_resources ) :

			global $post;

			while ( $_resources->have_posts() ) :

				$i++;

				$_resources->the_post();

				$_featured_image_url = get_the_post_thumbnail_url( null, 'full' );
				$prefix = 'pwl_conquip_products_';
				$products_product_url = esc_html( get_post_meta( $post->ID, $prefix . 'product_download', true) ); 


				// Search Result Markup
				$_content .= '<div class="search-result">';
				$_content .= '<div class="search-result-inner">';

				$_content .= '<a href="' . get_the_permalink() . '" title="Read more about our ' . get_the_title() .'">';
				$_content .= '<div class="search-bg-img" style="background-image: url(' . $_featured_image_url . ');"></div>';
				$_content .= '</a>';

				$_content .= '<div class="content-wrapper">';
					$_content .= '<a href="' . get_the_permalink() . '" title="Read more about our ' . get_the_title() .'">';
					$_content .= '<h3 class="archive-item">' . get_the_title() . '</h3>';
					$_content .= '<p class="archive-item-copy">' . get_the_excerpt() . '</p>';

					$_content .= '<div class="arrow-button">Explore</div>';

					if (! empty( $products_product_url ) ) {
						$_content .= '<a href="' . $products_product_url . '" class="download-button">Download</a>';
					}

					$_content .= '</a>';
				$_content .= '</div>';

				$_content .= '</div>';
				$_content .= '</div>';


				if ( $i === $columns ) {
					$i = 0;
					$_content .= '</div>';
					$_content .= '<div class="x-container">';
				}

			endwhile; wp_reset_postdata();

		elseif( $_search_resources ) :

			global $post;

			foreach( $_search_resources->posts as $post ) : setup_postdata( $post );

				$i++;
				$_featured_image_url = get_the_post_thumbnail_url( $post->ID, 'full' );
				$prefix = 'pwl_conquip_products_';
				$products_product_url = esc_html( get_post_meta( $post->ID, $prefix . 'product_download', true) ); 


				// Search Result Markup
				$_content .= '<div class="search-result">';
				$_content .= '<div class="search-result-inner">';

				$_content .= '<a href="' . get_the_permalink( $post->ID ) . '" title="Read more about our ' . get_the_title($post->ID) .'">';
				$_content .= '<div class="search-bg-img" style="background-image: url(' . $_featured_image_url . ');"></div>';
				$_content .= '</a>';

				$_content .= '<div class="content-wrapper">';
					$_content .= '<a href="' . get_the_permalink( $post->ID ) . '" title="Read more about our ' . get_the_title( $post->ID ) .'">';
					$_content .= '<h3 class="archive-item">' . get_the_title() . '</h3>';
					$_content .= '<p class="archive-item-copy">' . get_the_excerpt() . '</p>';
					$_content .= '<div class="arrow-button">Explore</div>';

					if (! empty( $products_product_url ) ) {
						$_content .= '<a href="' . $products_product_url . '" class="download-button">Download</a>';
					}

					$_content .= '</a>';
				$_content .= '</div>';

				$_content .= '</div>';
				$_content .= '</div>';


				if ( $i === $columns ) {
					$i = 0;
					$_content .= '</div>';
					$_content .= '<div class="x-container">';
				}

			endforeach; wp_reset_postdata();

		endif;

		$_content .= '</div>';

	else :

		$_content .= '<div class="x-container tab active" id="tab-1">';

		$_content .= '<div class="search-result">';

		$_content .= '<div class="search-result-inner">';

		if( ! empty( $_search_query ) ) :

			$_content .= '<p style="text-align:center;display:block;width:100%;">Sorry, we weren\'t able to find any content under the term: ' . $_search_query . '</p>';
			$_content .= '<p style="text-align:center;display:block;width:100%;">Please contact us for more informationm, or reset your search terms and try looking through some of our other products.</p>';

			$_content .= cornerstone_archive_pagination( 0, 0, $_search_query );

		else :

			$_content .= '<p style="text-align:center;">Sorry, we weren\'t able to find any content. Please contact us for more information.</p>';

		endif;

		$_content .= '</div>';

		$_content .= '</div>';

		$_content .= '</div>';

	endif;

	// ------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------  Start of the second tab for the search page ----------------------------------------------------
	// ------------------------------------------------------------------------------------------------------------------------------

	if( ( $_resources_explore && $_resources_explore->have_posts() ) || ( $_search_resources_explore && ( ! empty( $_search_resources_explore->posts ) ) ) ) :

		$i = 0;

		
		$_content .= '<div class="x-container tab" id="tab-2">';

		if( $_resources_explore ) :
			$_number_of_pages = $_resources_explore->max_num_pages;
		elseif( $_search_resources_explore ) :
			$_number_of_pages = $_search_resources_explore->max_num_pages;
		endif;

		if( $_number_of_pages ) :
			// Generate pagination, to fulfil $current_page, $total_pages, and $search_query.
			$_pagination = cornerstone_archive_pagination( $_paged_query_var, $_number_of_pages, $_search_query );
		else :
			$_pagination = false;
		endif;

		if( $_resources_explore ) :

			while ( $_resources_explore->have_posts() ) :

				$i++;

				$_resources_explore->the_post();

				$_featured_image_url = get_the_post_thumbnail_url( null, 'full' );

				// Search Result Markup
				$_content .= '<div class="search-result">';
				$_content .= '<div class="search-result-inner">';

				$_content .= '<a href="' . get_the_permalink() . '" title="Read more about our ' . get_the_title() .'">';
				$_content .= '<div class="search-bg-img" style="background-image: url(' . $_featured_image_url . ');"></div>';
				$_content .= '</a>';

				$_content .= '<div class="content-wrapper">';
					$_content .= '<a href="' . get_the_permalink() . '" title="Read more about our ' . get_the_title() .'">';
					$_content .= '<h3 class="archive-item">' . get_the_title() . '</h3>';
					$_content .= '<p class="archive-item-copy">' . get_the_excerpt() . '</p>';
					$_content .= '<div class="arrow-button">Explore</div>';
					$_content .= '</a>';
				$_content .= '</div>';

				$_content .= '</div>';
				$_content .= '</div>';

				if ( $i === $columns ) {
					$i = 0;
					$_content .= '</div>';
					$_content .= '<div class="x-container">';
				}

			endwhile; wp_reset_postdata();

		elseif( $_search_resources_explore ) :

			foreach( $_search_resources_explore->posts as $post ) : setup_postdata( $post );

				$i++;

				$_featured_image_url = get_the_post_thumbnail_url( $post->ID, 'full' );

				// Search Result Markup
				$_content .= '<div class="search-result">';
				$_content .= '<div class="search-result-inner">';

				$_content .= '<a href="' . get_the_permalink($post->ID) . '" title="Read more about our ' . get_the_title($post->ID) .'">';
				$_content .= '<div class="search-bg-img" style="background-image: url(' . $_featured_image_url . ');"></div>';
				$_content .= '</a>';

				$_content .= '<div class="content-wrapper">';
					$_content .= '<a href="' . get_the_permalink( $post->ID ) . '" title="Read more about our ' . get_the_title( $post->ID ) .'">';
					$_content .= '<h3 class="archive-item">' . get_the_title() . '</h3>';
					$_content .= '<p class="archive-item-copy">' . get_the_excerpt() . '</p>';
					$_content .= '<div class="arrow-button">Explore</div>';
					$_content .= '</a>';
				$_content .= '</div>';

				$_content .= '</div>';
				$_content .= '</div>';


				if ( $i === $columns ) {
					$i = 0;
					$_content .= '</div>';
					$_content .= '<div class="x-container">';
				}

			endforeach; wp_reset_postdata();

		endif;

		$_content .= '</div>';

	
		// Generate pagination, to fulfil $current_page, $total_pages, and $search_query.
		// $_pagination = cornerstone_archive_pagination( $_paged_query_var, $_number_of_pages, $_search_query );

		// If false, don't show.
		if( false !== $_pagination ) :

			$_content .= $_pagination;

		endif;

	else :

		$_content .= '<div class="x-container tab" id="tab-2">';

		$_content .= '<div class="search-result">';

		$_content .= '<div class="search-result-inner">';

		if( ! empty( $_search_query ) ) :

			$_content .= '<p style="text-align:center;display:block;width:100%;">Sorry, we weren\'t able to find any content under the term: ' . $_search_query . '</p>';
			$_content .= '<p style="text-align:center;display:block;width:100%;">Please contact us for more information, or reset your search terms and try looking through some of our other products.</p>';

			// $_content .= cornerstone_archive_pagination( 0, 0, $_search_query );

		else :

			$_content .= '<p style="text-align:center;">Sorry, we weren\'t able to find any content. Please contact us for more information.</p>';

		endif;

		$_content .= '</div>';

		$_content .= '</div>';

		$_content .= '</div>';

	endif;

	$_output = $_content;

	echo $_output;