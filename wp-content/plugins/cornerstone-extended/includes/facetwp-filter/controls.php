<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Element Controls: "FacetWP Filters"
 */

// Extra Colours
global $brand_colors; // Set in the cornerstone-extended.php file (to control all brand background colour options for a select)

/**
 * FacetWP Options for Facet and Templates
 */

// Check if Facet WP is activated
if ( class_exists( 'FacetWP' ) ){

	// FacetWP - get all Facets and Templates
	$facet_options		= json_decode( get_option( 'facetwp_settings' ), true );
	$fwp_facets_list		= array();

	/**
	 * FacetWP Facet Selector
	 */

	// Get FacetWP Facets
	for ( $i = 0; $i < sizeof( $facet_options['facets'] ); $i++ ) { 
		$fwp_facets_list[$i] = array( 'value' => $facet_options['facets'][$i]['source'].'|'.$facet_options['facets'][$i]['name'], 'label'	=> __( $facet_options['facets'][$i]['label'], 'pw-cornerstone-extended' ) );
	}

	/**
	 * FacetWP Template selector
	 * This dropdown will display a list of all the available FacetWP "templates".
	 * The section below is commented out, as it could be used in the next project.
	 */

	// Get FacetWP Templates
	// for ( $i = 0; $i < sizeof( $facet_options['templates'] ); $i++ ) { 
	// 	$fwp_templates[$i] = array( 'value' => $facet_options['templates'][$i]['source'], 'label'	=> __( $facet_options['templates'][$i]['label'], 'pw-cornerstone-extended' ) );
	// }

} else {
	$fwp_facets[0] 		= array( 'value' => '', 'label'	=> __( 'No Facets Found', 'pw-cornerstone-extended' ) );
	$fwp_templates[0] 	= array( 'value' => '', 'label'	=> __( 'No Templates Found', 'pw-cornerstone-extended' ) );
}

return array(

	'fwp_facet'	=> array(
		'type' 		=> 'select',
		'ui' 		=> array(
			'title'		=> __( 'Facet (Filter)', 'pw-cornerstone-extended' ),
			'tooltip'	=>__( 'Select the &ldquo;facet&rdquo; to use for filtering.', 'pw-cornerstone-extended' ),
		),
		'context'	=> 'content',
		'suggest'	=> 'default', // default choice selected from below
		'options'	=> array(
			'choices'	=> $fwp_facets_list,
		),
	),

	'show_filter_label'	=> array(
		'type'			=> 'toggle',
		'context'		=> 'content',
		'suggest'		=> '1',
		'ui'			=> array(
			'title'			=> __( 'Show Filter Label?', 'pw-cornerstone-extended' ),
			'tooltip'		=>__( 'Show or hide the Filter Label for this Facet.', 'pw-cornerstone-extended' ),
		),
	),

	'fwp_filter_label' => array(
		'type'    => 'text',
		'ui' => array(
			'title'   => __( 'Facet Filter Label', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Add the text for the Filter label.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
		'suggest' => __( 'Filter', 'pw-cornerstone-extended' ),
		'condition' => array(
			'show_filter_label' => true,
		),
	),

	'fwp_label_position' => array(
		'type'	=> 'choose',
		'ui'	=> array(
			'title'		=> __( 'Filter Label Position', 'pw-cornerstone-extended' ),
			'tooltip'	=> __( 'Place the Filter Label either above or beside the Filters.', 'pw-cornerstone-extended' ),
			'divider'	=> false,
		),
		'context'	=> 'content',
		'suggest'	=> 'above',
		'options'	=> array(
			'columns'	=> '2',
			'choices'	=> array(
				array( 'value' => 'above',   'tooltip' => __( 'Above', 'pw-cornerstone-extended' ),   'icon' => fa_entity( 'arrow-up' ) ),
				array( 'value' => 'left', 'tooltip' => __( 'Left', 'pw-cornerstone-extended' ), 'icon' => fa_entity( 'arrow-left' ) ),
				// array( 'value' => 'below', 'tooltip' => __( 'Below', 'pw-cornerstone-extended' ), 'icon' => fa_entity( 'arrow-left' ) ),
			),
		),
	),
	

	// 'fwp_type'	=> array(
	// 	'type' 		=> 'select',
	// 	'ui' 		=> array(
	// 		'title'		=> __( 'Facet Type', 'pw-cornerstone-extended' ),
	// 		'tooltip'	=>__( 'Select the type of Facet display type to use (e.g. radio button or checkbox).', 'pw-cornerstone-extended' ),
	// 	),
	// 	'context'	=> 'content',
	// 	'suggest'	=> 'radio', // default choice selected from below
	// 	'options'	=> array(
	// 		'choices'	=> array(
	// 			array( 'value' => 'radio',	'label'	=> __( 'Radio Button', 'pw-cornerstone-extended' ) ),
	// 			array( 'value' => 'checkbox',	'label'	=> __( 'Checkbox', 'pw-cornerstone-extended' ) ),
	// 			array( 'value' => 'large',	'label'	=> __( 'Large', 'pw-cornerstone-extended' ) ),
	// 		),
	// 	),
	// ),

	'button_color' => array(
		'type' => 'select',
		'ui' => array(
			'title'   => __( 'Button Color', 'pw-cornerstone-extended' ),
			'tooltip' =>__( 'Choose a custom color for the FacetWP buttons.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
		'suggest' => 'primary-blue',
		'options' => array(
			'choices' => $brand_colors,
		),
	),

	'show_reset_button'	=> array(
		'type'			=> 'toggle',
		'context'		=> 'content',
		'suggest'		=> '',
		'ui'			=> array(
			'title'			=> __( 'Show Reset ("View All") Button?', 'pw-cornerstone-extended' ),
			'tooltip'		=>__( 'Show or hide the Reset Filters ("View All") button.', 'pw-cornerstone-extended' ),
		),
	),

	'reset_button_text' => array(
		'type'    => 'text',
		'ui' => array(
			'title'   => __( 'Reset Button Text', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Change the &ldquoView All&rdquo button text.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
		'suggest' => __( 'View All', 'pw-cornerstone-extended' ),
		'condition' => array(
			'show_reset_button' => true,
		),
	),
	

	/**
	 * Show/Hide FacetWP SortBy Dropdown
	 * IMPORTANT: Commented out section below is saved for next project, just in case we need it.
	 * It's not used on this project. Uncomment to use this feature.
	 */
	// 'show_sort_options'	=> array(
	// 	'type'			=> 'toggle',
	// 	'context'		=> 'content',
	// 	'suggest'		=> '1',
	// 	'ui'			=> array(
	// 		'title'			=> __( 'Show Sorting Options', 'pw-cornerstone-extended' ),
	// 		'tooltip'		=>__( 'Show or hide the FacetWP Sorting Options dropdown.', 'pw-cornerstone-extended' ),
	// 	),
	// ),

	/**
	 * FacetWP Template file selector
	 * This dropdown will display a list of all the available FacetWP "templates".
	 * The section below is commented out, as it could be used in the next project.
	 */


	// 'use_template'	=> array(
	// 	'type'			=> 'toggle',
	// 	'context'		=> 'content',
	// 	'suggest'		=> '',
	// 	'ui'			=> array(
	// 		'title'			=> __( 'Use Template', 'pw-cornerstone-extended' ),
	// 		'tooltip'		=>__( 'Enable this option if you want to use a specific FacetWP &ldquo;template&rdquo;.', 'pw-cornerstone-extended' ),
	// 	),
	// ),
	
	// 'fwp_template'	=> array(
	// 	'type' 		=> 'select',
	// 	'ui' 		=> array(
	// 		'title'		=> __( 'FacetWP Template', 'pw-cornerstone-extended' ),
	// 		'tooltip'	=>__( 'Select the template to use with this facet.', 'pw-cornerstone-extended' ),
	// 	),
	// 	'context'	=> 'content',
	// 	'suggest'	=> 'normal', // default choice selected from below
	// 	'options'	=> array(
	// 		'choices'	=> $fwp_templates,
	// 	),
	// 	'condition' => array(
	// 		'use_template' => true,
	// 	),
	// ),

);
