<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Element Controls: "Slick Slider Slide Item New"
 */

$transparent = array( 'value' => '',	'label'	=> __( 'None', 'pw-cornerstone-extended' ) );

return array(

/**
 * SLIDER TEXT CONTENT
 */

	// 'use_text'	=> array(
	// 	'type'			=> 'toggle',
	// 	'context'		=> 'content',
	// 	'suggest'		=> '1',
	// 	'ui'			=> array(
	// 		'title'			=> __( 'Use Slide Text', 'pw-cornerstone-extended' ),
	// 		'tooltip'		=> __( 'Enable or disable the use of Text on this slide.', 'pw-cornerstone-extended' ),
	// 	),
	// ),

	// 'slide_text' => array(
	// 	'type'    => 'textarea',
	// 	'ui' => array(
	// 		'title'   => __( 'Slide Text', 'pw-cornerstone-extended' ),
	// 		'tooltip' => __( 'Add the text to use with this Slide.', 'pw-cornerstone-extended' ),
	// 	),
	// 	'context' => 'content',
	// 	'suggest' => __( 'Enter your slide text here.'."\n\n".'Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Proin eget tortor risus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.', 'pw-cornerstone-extended' ),
	// 	'condition' => array(
	// 		'use_text'	=> true,
	// 	),
	// ),

/**
 * Background Image and Youtube Embed
 */



	'slide_bg_image' => array(
		'type'    => 'image',
		'ui' => array(
			'title'   => __( 'Background Image', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Select the background image to use for this slide.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
	),	

	'slide_title' => array(
		'type'    => 'text',
		'ui' => array(
			'title'   => __( 'Slider Title', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Please enter the code from the end of a vimeo url to use as a background.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
	),
	'slide_text' => array(
		'type'    => 'textarea',
		'ui' => array(
			'title'   => __( 'Slider Text', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Please enter the code from the end of a vimeo url to use as a background.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
	),
	'slide_cta_title' => array(
		'type'    => 'text',
		'ui' => array(
			'title'   => __( 'Slider CTA Title', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Please enter the code from the end of a vimeo url to use as a background.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
	),
	'slide_cta_url' => array(
		'type'    => 'text',
		'ui' => array(
			'title'   => __( 'Slider CTA URL', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Please enter the code from the end of a vimeo url to use as a background.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
	),
);


