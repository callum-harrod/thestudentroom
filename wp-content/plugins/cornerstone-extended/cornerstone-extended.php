<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/*
Plugin Name: Cornerstone - Extended
Plugin URI:
Description: Extending the Cornerstone WordPress page builder to have additional functionality.
Author: Pragmatic Web (ZB)
Author URI: http://www.pragmatic-web.co.uk
Version: 0.1.0
*/

define( 'PWL_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'PWL_PLUGIN_URL', plugin_dir_url( __FILE__ ) );


/**
 * Set up Cornerstone Extended
 * Checks to see if the main Cornerstone plugin exists before loading new custom Elements.
 */

add_action( 'cornerstone_register_elements', 'pw_cornerstone_extended_register_elements' ); // Trigger Cornerstone elements action

function pw_cornerstone_extended_register_elements() {

	// Make sure Cornerstone exists
	if ( class_exists( 'Cornerstone_Plugin_Base' ) ){

		/**
		 * Register a new element with 'cornerstone_register_element()'
		 * @param  $class_name Name of the class you've created in definition.php
		 * @param  $name       slug name of the element. "alert" for example.
		 * @param  $path       Path to the folder containing a definition.php file.
		 */

		cornerstone_register_element( 'PWL_Case_Studies_Archive', 'case-studies-archive', PWL_PLUGIN_PATH . 'includes/case-studies-archive' );

		if ( class_exists( 'FacetWP' ) ){
			cornerstone_register_element( 'PWL_FacetWP', 'facetwp-posts', PWL_PLUGIN_PATH . 'includes/facetwp-posts' );
			cornerstone_register_element( 'PWL_FacetWP_Filter', 'facetwp-filter', PWL_PLUGIN_PATH . 'includes/facetwp-filter' );
		}

		// cornerstone_register_element( 'PWL_Slick_Slider', 'slick-slider', PWL_PLUGIN_PATH . 'includes/slick-slider' );
		// cornerstone_register_element( 'PWL_Slick_Slider_Slide_Item', 'slick-slider-slide-item', PWL_PLUGIN_PATH . 'includes/slick-slider-slide-item' );

		cornerstone_register_element( 'PWL_Slick_Slider_New', 'slick-slider-new', PWL_PLUGIN_PATH . 'includes/slick-slider-new' );
		cornerstone_register_element( 'PWL_Slick_Slider_Slide_Item_New', 'slick-slider-slide-item-new', PWL_PLUGIN_PATH . 'includes/slick-slider-slide-item-new' );
		cornerstone_register_element( 'PWL_CTA_Buttons', 'cta-buttons', PWL_PLUGIN_PATH . 'includes/cta-buttons' );

	}
}


// Enqueue plugin styles
add_action( 'wp_enqueue_scripts', 'pw_cornerstone_extended_enqueue' );

function pw_cornerstone_extended_enqueue() {

	// Load the Cornerstone Extended Styles
	wp_enqueue_style( 'pw-cs-extended-slider', PWL_PLUGIN_URL . 'assets/slick/slick.css', '', '0.1.0' );
	wp_enqueue_style( 'pw-cs-extended-style', PWL_PLUGIN_URL . 'assets/css/cornerstone-extended.css', array( 'theme-style' ), '0.1.0' );
	wp_enqueue_script( 'pw-cs-extended-slider-js', PWL_PLUGIN_URL . 'assets/slick/slick.min.js', array( 'jquery'), '0.1.0', true );

}

/**
 * FacetWP - Query extend
 * Note: function enables specific queries to be used by FacetWP, when multiple queries appear on the page.
 * See: https://facetwp.com/documentation/facetwp_is_main_query/
 */

function pw_cornerstone_extended_facetwp_is_main_query( $is_main_query, $query ) {
    if ( isset( $query->query_vars['facetwp'] ) ) {
        $is_main_query = (bool) $query->query_vars['facetwp'];
    }
    return $is_main_query;
}

add_filter( 'facetwp_is_main_query', 'pw_cornerstone_extended_facetwp_is_main_query', 10, 2 );

/**
 * FacetWP - Pager HTML Output
 * Amend this filter function to control the output of the FacetWP 'pager'
 */

function pw_cornerstone_extended_facetwp_pager_html( $output, $params ) {

	parse_str( $_SERVER["QUERY_STRING"], $query_array );
	$active_page 	= ( !empty($query_array['fwp_paged']) ) ? $query_array['fwp_paged'] : '';
	$output = '';

	for ($i=1; $i <= $params['total_pages']; $i++) {

		$is_active = ( ( $active_page == $i ) || ( ('' == $active_page ) && ( $i == 1 ) ) ) ? ' active' : '' ;
		$output .= '<a class="facetwp-page button custom-button button-facetwp'.$is_active.'" data-page="'.$i.'">'.$i.'</a>';
	}

	return $output;

}

add_filter( 'facetwp_pager_html', 'pw_cornerstone_extended_facetwp_pager_html', 10, 2 );

/**
 * FacetWP - Facet Output
 * Add the "View All" button to the list of Facet categories
 */

function pw_cornerstone_extended_facetwp_facet_html( $output, $params ) {

    $output = '<div class="facetwp-radio view_all" onclick="FWP.reset()">All</div>'.$output;
    return $output;

}

// add_filter( 'facetwp_facet_html', 'pw_cornerstone_extended_facetwp_facet_html', 10, 2 );

/**
 * Function: 'pwl_cse_custom_colours()'
 * $toggle_name (* Required). This is the name of the toggle used with the pwl_cse_colour_selector()
 * $item_name (Optional). Name of the item (e.g Heading or Background) to be used with the element label.
 */
function pwl_cse_custom_colours ( $condition = '', $item_name = '', $suffix_text = '' ){

	$item_name = ( '' != $item_name ) ? $item_name . ' ' : '' ;

	$custom_colour_output = array(
		'type'    => 'color',
		'ui' => array(
			'title'   => __( $item_name . 'Colour (Custom)', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Select a Custom ' . $item_name . ' colour' . $suffix_text. '.', 'pw-cornerstone-extended' ),
		),
		'context' 	=> 'content',
		// 'condition' => array(
		// 	$toggle_name	=> 'custom',
		// ),
	);

	if ( '' != $condition ){
		$custom_colour_output['condition'] = $condition;
	}

	return $custom_colour_output;
}

$brand_colors = array(
	array( 'value' => 'pink',		'label' => __( 'Pink', 'pw-cornerstone-extended' ) ),
	array( 'value' => 'purple',		'label' => __( 'Purple', 'pw-cornerstone-extended' ) ),
	array( 'value' => 'peach',	'label' => __( 'Peach', 'pw-cornerstone-extended' ) ),
	array( 'value' => 'blue',		'label' => __( 'Blue', 'pw-cornerstone-extended' ) ),
	array( 'value' => 'green',	'label' => __( 'Green', 'pw-cornerstone-extended' ) ),
	array( 'value' => 'grey',			'label' => __( 'Dark Grey', 'pw-cornerstone-extended' ) ),
	array( 'value' => 'light-grey',			'label' => __( 'Light Grey', 'pw-cornerstone-extended' ) ),
	// array( 'value' => 'primary-blue',		'label' => __( 'Primary Blue', 'pw-cornerstone-extended' ) ),
	// array( 'value' => 'primary-green',		'label' => __( 'Primary Green', 'pw-cornerstone-extended' ) ),
	// array( 'value' => 'primary-turquoise',	'label' => __( 'Primary Turquoise', 'pw-cornerstone-extended' ) ),
	// array( 'value' => 'secondary-blue',		'label' => __( 'Secondary Mid-Blue', 'pw-cornerstone-extended' ) ),
	// array( 'value' => 'secondary-green',	'label' => __( 'Secondary Mid-Green', 'pw-cornerstone-extended' ) ),
	// array( 'value' => 'secondary-purple',	'label' => __( 'Secondary Purple', 'pw-cornerstone-extended' ) ),
	// array( 'value' => 'light-gray',			'label' => __( 'Secondary Light Grey', 'pw-cornerstone-extended' ) ),
	// array( 'value' => 'dark-gray',			'label' => __( 'Secondary Dark Grey', 'pw-cornerstone-extended' ) ),
);

/**
 * Extra Colours - Add black and white to the list of global colours
 * Used for Post Content Title heading colour
 */
$brand_colors_extra = $brand_colors;
$brand_white = array( 'value' => 'white',	'label'	=> __( 'White', 'pw-cornerstone-extended' ) );
array_unshift( $brand_colors_extra, $brand_white );
$brand_black = array( 'value' => 'black',	'label'	=> __( 'Black', 'pw-cornerstone-extended' ) );
array_push( $brand_colors_extra, $brand_black );

/**
 * Function: 'pwl_cse_colour_selector()'
 * $condition (Optional). Array - used if the Colour Selector is dependent on another toggle or Element.
 * $item_name (Optional). Name of the item (e.g Heading or Background) to be used with the element label.
 * $show_custom (Optional). Boolean to either enable or disable the 'Custom' select option.
 */

function pwl_cse_colour_selector( $condition = '', $item_name = '', $show_custom = true , $suggest = '' ){

	$item_name = ( '' != $item_name ) ? $item_name . ' ' : '' ;

	$color_selector_output = array(
		'type' 		=> 'select',
		'ui' 		=> array(
			'title'		=> __( $item_name . 'Colour', 'pw-cornerstone-extended' ),
			'tooltip'	=> __( 'Select whether to use a ' . $item_name .' colour.', 'pw-cornerstone-extended' ),
		),
		'context'	=> 'content',
		'suggest'	=> '', // default choice selected from below
		'options'	=> array(
			'choices'	=> array(
				array( 'value' => '',	'label'	=> __( 'None', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'brand',	'label'	=> __( 'Brand Colours', 'pw-cornerstone-extended' ) ),
			),
		),
	);

	if ( $show_custom ){
		$color_selector_output['options']['choices'][] = array( 'value' => 'custom',	'label'	=> __( 'Custom Colours', 'pw-cornerstone-extended' ) );
	}

	if ( '' != $condition ){
		$color_selector_output['condition'] = $condition;
	}

	if ( '' != $suggest ){
		$color_selector_output['suggest'] = $suggest;
	}

	return $color_selector_output;
}

/**
 * Function: 'pwl_cse_brand_colours()'
 * $toggle_name (* Required). This is the name of the toggle used with the pwl_cse_colour_selector()
 * $brand_colors (* Required). The brand colours array variable.
 * $suggested (Optional). Suggested colour - should match one of the values in the $brand_colors array.
 * $item_name (Optional). Name of the item (e.g Heading or Background) to be used with the element label.
 * $suffix_text (Optional). Text that appears at the end in the Tooltip.
 */

function pwl_cse_brand_colours( $condition = '', $brand_colors, $suggested ='', $item_name = '', $suffix_text = '' ){

	$item_name = ( '' != $item_name ) ? $item_name . ' ' : '' ;

	$brand_color_output = array(
		'type' 		=> 'select',
		'ui' 		=> array(
			'title'		=> __( $item_name . 'Colour (Brand)', 'pw-cornerstone-extended' ),
			'tooltip'	=> __( 'Select a Brand ' . $item_name . ' colour' . $suffix_text. '.', 'pw-cornerstone-extended' ),
		),
		'context' 	=> 'content',
		'suggest'	=> $suggested, // default choice selected from below
		'options'	=> array(
			'choices'	=> $brand_colors,
		),
		// 'condition' => array(
		// 	$toggle_name => 'brand',
		// ),
	);

	if ( '' != $condition ){
		$brand_color_output['condition'] = $condition;
	}

	return $brand_color_output;
}
