<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to X in this file.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Enqueue Parent Stylesheet
//   02. Additional Functions
// =============================================================================

// Enqueue Parent Stylesheet
// =============================================================================

add_filter( 'x_enqueue_parent_stylesheet', '__return_true' );

/**
 * Styles and Scripts
 *
 * @since 1.0.0*/

add_action(
	'wp_enqueue_scripts', function() {

		// Get Theme Version
		$theme_version = wp_get_theme()->Version;

		// Local Dev - use a "new" version every page load
		if ( defined( 'LOCAL_DEV' ) && LOCAL_DEV ) {
			$theme_version = time();
		}

		// Make links protocol relative
		$template_directory_uri = str_replace( 'http:', '', get_stylesheet_directory_uri() );
		$template_directory_uri = str_replace( 'https:', '', $template_directory_uri );

		/**
	* Note: At present, all styles are in the Customizer / Custom / CSS for the theme.
	* There are no custom stylesheets or scripts, so excluding the next 2 lines. The scripts.js file does not exist.
	*/
		wp_dequeue_style( 'x-child' );
		wp_deregister_style( 'x-child' );

		// Theme Style
		wp_enqueue_style( 'theme-style', $template_directory_uri . '/dist/styles/styles.min.css', array(), $theme_version );

		wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', $theme_version );

		// Theme Scripts
		wp_enqueue_script( 'theme-scripts', $template_directory_uri . '/dist/scripts/scripts.min.js', array( 'jquery' ), $theme_version, true );

	}, 10
);

// Add TinyMCE options, you can find out more about these options here: http://www.tinymce.com/wiki.php/Configuration:formats
function wpb_mce_buttons_2($buttons) {
	array_unshift($buttons, 'styleselect');
	return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');

/*
* Callback function to filter the MCE settings
*/

function override_mce_options($initArray) {
    $opts = '*[*]';
    $initArray['valid_elements'] = $opts;
    $initArray['extended_valid_elements'] = $opts;
    return $initArray;
} 
add_filter('tiny_mce_before_init', 'override_mce_options');

function my_mce_before_init_insert_formats( $init_array ) {  

// Define the style_formats array

	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => 'Button',  
			'selector' => 'a',  
			'classes' => 'button',
		),  
		array(  
			'title' => 'Scroll Button',  
			'selector' => 'a',  
			'classes' => 'button scroll',
		),
		array(  
			'title' => 'Span',  
			'block' => 'span',  
			'classes' => 'text-span',
			'wrapper' => true,
		),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

function my_theme_add_editor_styles() {
	add_editor_style( 'css/admin-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );

function unhide_kitchensink( $args ) { $args['wordpress_adv_hidden'] = false; return $args; } add_filter( 'tiny_mce_before_init', 'unhide_kitchensink' );

// Allow SVG

function cc_mime_types($mimes) {
$mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// Custom Functions

function after_masthead_begin() {
	echo '<h1>Heading 1 - After Masthead</h1>';
}

add_action( 'init', 'header_actions' );
function header_actions() {
	add_action( 'x_after_masthead_begin', 'after_masthead_begin', 10 );
}

// add fifth widget area to theme

function footer_additional_widget() {

	register_sidebar( array(
		'name'          => 'Footer 5',
		'id'            => 'footer-5',
		'description' => __( 'Widgetized footer area.', 'tsr' ),
		'before_widget' => '<div class="widget widget_nav_menu">',
		'after_widget'  => '</div>',
		'before_title'  => '<h4 class="h-widget">',
		'after_title'   => '</h4>',
	) );

}
add_action( 'widgets_init', 'footer_additional_widget' );

function child_theme_head_script() { ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<?php }
add_action( 'wp_head', 'child_theme_head_script' );
