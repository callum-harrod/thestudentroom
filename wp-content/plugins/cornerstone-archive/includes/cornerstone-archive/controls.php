<?php

/**
 * Element Controls
 */

$_post_types = get_post_types();

$_custom_post_list = [];

foreach ( $_post_types as $_post_type ) {
	$_custom_post_list[] = [
		'value' => $_post_type,
		'label' => $_post_type,
	];
}

return array(

	'post_type' => [
		'type' => 'select',
		'ui' => [
			'title' => __( 'Post Type', 'cornerstone-archive' ),
		],
		'options' => [
			'choices' => $_custom_post_list,
		],
	],

	'count' => array(
		'type' => 'text',
		'ui' => array(
			'title'   => __( 'Products per Page', 'cornerstone-archive' ),
		),
		'context' => 'content',
	),

	'columns' => [
		'type' => 'select',
		'ui' => [
			'title' => __( 'Columns', 'cornerstone-archive' ),
		],
		'options' => [
			'choices' => [
				[ 'value' => 1, 'label' => __( '1', 'cornerstone-archive' ) ],
				[ 'value' => 2, 'label' => __( '2', 'cornerstone-archive' ) ],
				[ 'value' => 3, 'label' => __( '3', 'cornerstone-archive' ) ],
				[ 'value' => 4, 'label' => __( '4', 'cornerstone-archive' ) ],
			],
		],
	],

);
