jQuery(document).ready(function($) {
	$('.partners-slider > div').slick({
		speed: 6000,
		autoplay: true,
		autoplaySpeed: 0,
		centerMode: true,
		cssEase: 'linear',
		slidesToShow: 1,
		slidesToScroll: 1,
		variableWidth: true,
		infinite: true,
		arrows: false,
		buttons: false,
	});
});
