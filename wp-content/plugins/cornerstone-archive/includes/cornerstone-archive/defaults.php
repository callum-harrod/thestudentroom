<?php

/**
 * Defaults Values
 */

return array(
	'id'			=> '',
	'class'			=> '',
	'style'			=> '',
	'count'			=> 12,
	'post_type'		=> 'post',
	'columns'		=> 4,
);