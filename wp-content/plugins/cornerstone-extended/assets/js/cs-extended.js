jQuery(document).foundation();
jQuery(document).foundation({
    accordion: {
        callback: function(accordion) {                           
            jQuery(document).foundation('equalizer', 'reflow');
        }
    },
    equalizer: {
        equalize_on_stack: true,
        act_on_hidden_el: true,
        after_height_change: function() {
            jQuery(document).foundation('accordion', 'reflow');
        }
    }
});