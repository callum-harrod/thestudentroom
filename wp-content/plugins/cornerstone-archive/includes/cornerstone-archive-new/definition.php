<?php

/**
 * Element Definition
 */

class Cornerstone_Archive_New {

	public function ui() {
		return array(
		'title'       => __( 'Cornerstone Archive New', 'cornerstone-archive' ),
		'autofocus' => array(
			'heading' => 'h4.cornerstone-archive-heading',
			'content' => '.cornerstone-archive',
		),
		'icon_group' => 'cornerstone-archive',
		);
	}

	public function update_build_shortcode_atts( $atts ) {

		// This allows us to manipulate attributes that will be assigned to the shortcode
		// Here we will inject a background-color into the style attribute which is
		// already present for inline user styles
		if ( ! isset( $atts['style'] ) ) {
			$atts['style'] = '';
		}

		if ( isset( $atts['background_color'] ) ) {
			$atts['style'] .= ' background-color: ' . $atts['background_color'] . ';';
			unset( $atts['background_color'] );
		}

		return $atts;

	}

}

function custom_query_vars_filter_new($vars) {
	$vars[] = 'archive_page';
	return $vars;
}
add_filter( 'query_vars', 'custom_query_vars_filter_new' );
