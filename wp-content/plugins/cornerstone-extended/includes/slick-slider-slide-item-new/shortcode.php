<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Shortcode handler: "Slick Slider Slide Item New"
 */

// Note: $slider_height is defined in the parent folder 'slick-slider' and *linked* to the child (see definition.php)
$post_id 		= get_the_id();
$id				= ( '' != $id ) ? 'id="' . esc_attr( $id ) . '"' : '';
$class			= ( '' != $class ) ? ' ' . esc_attr( $class ) : '';
$min_height		= 'min-height:'.$slider_height.'px; ';


global $_multislider_slide_context;

if ( 'top' === $_multislider_slide_context )  {

	if ( '' != $slide_bg_video ) {

		// Start outputting the shortcode
		$output_shortcode 		 = '<div '.$id.' class="pwl_cs_slide ' . $class . ' style="">';
		$output_shortcode		.= '<iframe width="100%" height="300" src="https://player.vimeo.com/video/' . $slide_bg_video . '"></iframe>';
		$output_shortcode 		.= '</div>'; // .pwl_cse_slide

	} else {

		// Start outputting the shortcode
		$output_shortcode 		 = "<div $id class='slider-background-image' style='background-image: url(\"$slide_bg_image \");'>";
			$output_shortcode 		.= '<div class="row">';
				$output_shortcode 		.= '<div class="text_container column">';
					$output_shortcode		.= '<div class="inner_content columm">';

						$output_shortcode .= '<div class="hero-wysiwyg">';

							$output_shortcode .= '<h1>' . $slide_title . '</h1>';
							$output_shortcode .= wp_kses_post( apply_filters( 'the_content', $slide_text ) );

							if ( '' != $slide_cta_url && '' != $slide_cta_title ) {
								$output_shortcode .= '<a href="' . $slide_cta_url . '" class="cta">' . $slide_cta_title . '</a>';
							}

						$output_shortcode .= '</div>';

					$output_shortcode		.= '</div>'; // .inner_content
				$output_shortcode		.= '</div>'; // .text_container
			$output_shortcode		.= '</div>'; // .row
		$output_shortcode 		.= '</div>'; // .pwl_cse_slide

	}

	echo $output_shortcode;
}