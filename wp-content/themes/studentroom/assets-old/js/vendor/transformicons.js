var $ = jQuery.noConflict();

// Read More Functionality
$(document).ready(function(){
    $('.read-more-link').bind('click touchstart',function(event){
        
        $( '.read-more-content' ).toggleClass( "active" );
        $( this ).toggleClass( "active" );

        if ( $( this ).hasClass( "active" )) {
            $( this ).text('Show Less...');
        } else {
            $( this ).text('Read More...');
        }
        event.preventDefault();
    });
});

// Menu Toggle
$(document).ready(function(){
  $('div.mobile-menu-button button').bind('click',function(event){
    $( '.navigation-wrapper' ).toggleClass( "active" );
    $( '.navigation-bar' ).toggleClass( "active" );
    $( 'div.mobile-menu-button' ).toggleClass( "active" );

    $( 'body' ).toggleClass( "lock" );
  });
});

// Search Toggle
$(document).ready(function(){
  $('#search-toggle').bind('click',function(event){
    $( '.offcanvas-search' ).toggleClass( "active" );
    $( '#search-toggle' ).toggleClass( "active" );

    if ($(window).width() > 1240) {
       $( ".offcanvas-search #archive_query" ).focus();
    }
    $( 'body' ).toggleClass( "lock" );
  });

  $('#close-search').bind('click',function(event){
    $( '.offcanvas-search' ).toggleClass( "active" );
    $( '#search-toggle' ).toggleClass( "active" );
    $( 'body' ).toggleClass( "lock" );
  });
});

$(document).keyup(function(e) {
    if (e.keyCode == 27 && $('.offcanvas-search').hasClass( "active" )) {
        $( '.offcanvas-search' ).toggleClass( "active" );
        $( '#search-toggle' ).toggleClass( "active" );
        $( '.tcon' ).toggleClass('tcon-transform');
    }

    if (e.keyCode == 27 && $('.navigation-bar').hasClass( "active" )) {
      $( '.navigation-wrapper' ).toggleClass( "active" );
      $( '.navigation-bar' ).toggleClass( "active" );
      $( 'div.mobile-menu-button' ).toggleClass( "active" );
      $( '.tcon' ).toggleClass('tcon-transform');
    }
});

// Menu Detect 

$(document).ready(function(){
    var windowWidth = $(window).width();
    var windowMid = $(window).width() / 2;

    $('#menu-primary-menu .sub-menu').each(function(ev){
        var parentOffset = $( this ).parent().offset().left;
        if ( parentOffset > windowMid ) {
            $( this ).toggleClass( "place-right" );
        }
    });

  window.addEventListener('resize', function () {

    $('#menu-primary-menu .sub-menu').each(function(ev){
        var parentOffset = $( this ).parent().offset().left;
        if ( parentOffset > windowMid ) {
            $( this ).toggleClass( "place-right" );
        }
    });
  })
});

// Transformicons

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD module
    define(factory);
  } else if (typeof exports === 'object') {
    // CommonJS-like environment (i.e. Node)
    module.exports = factory();
  } else {
    // Browser global
    root.transformicons = factory();
  }
}(this || window, function () {

  // ####################
  // MODULE TRANSFORMICON
  // ####################
  'use strict';

  var
    tcon = {}, // static class
    _transformClass = 'tcon-transform',

    // const
    DEFAULT_EVENTS = {
      transform : ['click'],
      revert : ['click']
    };

  // ##############
  // private methods
  // ##############

  /**
  * Normalize a selector string, a single DOM element or an array of elements into an array of DOM elements.
  * @private
  *
  * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements
  * @returns {array} Array of DOM elements
  */
  var getElementList = function (elements) {
    if (typeof elements === 'string') {
      return Array.prototype.slice.call(document.querySelectorAll(elements));
    } else if (typeof elements === 'undefined' || elements instanceof Array) {
      return elements;
    } else {
      return [elements];
    }
  };

  /**
  * Normalize a string with eventnames separated by spaces or an array of eventnames into an array of eventnames.
  * @private
  *
  * @param {(string|array)} elements - String with eventnames separated by spaces or array of eventnames
  * @returns {array} Array of eventnames
  */
  var getEventList = function (events) {
    if (typeof events === 'string') {
      return events.toLowerCase().split(' ');
    } else {
      return events;
    }
  };

  /**
  * Attach or remove transformicon events to one or more elements.
  * @private
  *
  * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be toggled
  * @param {object} [events] - An Object containing one or more special event definitions
  * @param {boolean} [remove=false] - Defines wether the listeners should be added (default) or removed.
  */
  var setListeners = function (elements, events, remove) {
    var
      method = (remove ? 'remove' : 'add') + 'EventListener',
      elementList = getElementList(elements),
      currentElement = elementList.length,
      eventLists = {};

    // get events or use defaults
    for (var prop in DEFAULT_EVENTS) {
      eventLists[prop] = (events && events[prop]) ? getEventList(events[prop]) : DEFAULT_EVENTS[prop];
    }
    
    // add or remove all events for all occasions to all elements
    while(currentElement--) {
      for (var occasion in eventLists) {
        var currentEvent = eventLists[occasion].length;
        while(currentEvent--) {
          elementList[currentElement][method](eventLists[occasion][currentEvent], handleEvent);
        }
      }
    }
  };

  /**
  * Event handler for transform events.
  * @private
  *
  * @param {object} event - event object
  */
  var handleEvent = function (event) {
    tcon.toggle(event.currentTarget);
  };

  // ##############
  // public methods
  // ##############

  /**
  * Add transformicon behavior to one or more elements.
  * @public
  *
  * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be toggled
  * @param {object} [events] - An Object containing one or more special event definitions
  * @param {(string|array)} [events.transform] - One or more events that trigger the transform. Can be an Array or string with events seperated by space.
  * @param {(string|array)} [events.revert] - One or more events that trigger the reversion. Can be an Array or string with events seperated by space.
  * @returns {transformicon} transformicon instance for chaining
  */
  tcon.add = function (elements, events) {
    setListeners(elements, events);
    return tcon;
  };

  /**
  * Remove transformicon behavior from one or more elements.
  * @public
  *
  * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be toggled
  * @param {object} [events] - An Object containing one or more special event definitions
  * @param {(string|array)} [events.transform] - One or more events that trigger the transform. Can be an Array or string with events seperated by space.
  * @param {(string|array)} [events.revert] - One or more events that trigger the reversion. Can be an Array or string with events seperated by space.
  * @returns {transformicon} transformicon instance for chaining
  */
  tcon.remove = function (elements, events) {
    setListeners(elements, events, true);
    return tcon;
  };

  /**
  * Put one or more elements in the transformed state.
  * @public
  *
  * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be transformed
  * @returns {transformicon} transformicon instance for chaining
  */
  tcon.transform = function (elements) {
    getElementList(elements).forEach(function(element) {
      element.classList.add(_transformClass);
    });
    return tcon;
  };

  /**
  * Revert one or more elements to the original state.
  * @public
  *
  * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be reverted
  * @returns {transformicon} transformicon instance for chaining
  */
  tcon.revert = function (elements) {
    getElementList(elements).forEach(function(element) {
      element.classList.remove(_transformClass);
    });
    return tcon;
  };
  
  /**
  * Toggles one or more elements between transformed and original state.
  * @public
  *
  * @param {(string|element|array)} elements - Selector, DOM element or Array of DOM elements to be toggled
  * @returns {transformicon} transformicon instance for chaining
  */
  tcon.toggle = function (elements) {
    getElementList(elements).forEach(function(element) {
      tcon[element.classList.contains(_transformClass) ? 'revert' : 'transform'](element);
    });
    return tcon;
  };

  return tcon;
}));

transformicons.add('.tcon');

// Custom Slider Changes

$(document).ready(function(){
  $('.flex-prev i').addClass('x-icon-chevron-circle-left').removeClass('x-icon-chevron-left');
  $('.flex-next i').addClass('x-icon-chevron-circle-right').removeClass('x-icon-chevron-right');
});