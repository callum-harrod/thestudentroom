<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Shortcode handler: "Custom Button"
 */

$id				= ( $id    != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
$class			= ( $class != '' ) ? ' ' . esc_attr( $class ) : '';
$style			= ( $style != '' ) ? 'style="' . $style . '"' : '';
$size			= ( $size  != 'regular' ) ? ' ' . $size : '';
$block				= ( $block				== '1'  ) ? ' expanded' : '';
$button_bg_colour	= ( $button_bg_colour	!= ''  ) ? ' '.$button_bg_colour : '';
$href				= ( $href				!= ''      ) ? $href : '#';
$target				= ( $target				== 'blank' ) ? ' target="_blank"' : '';
$unicode			= fa_unicode( $icon ); // Cornerstone uses it's own function to convert the $icon text to a symbol

switch ( $button_align ) {
	case 'left' :
		$button_align 	= ' text-left';
		break;
	case 'right' :
		$button_align = ' text-right';
		break;
	default :
		$button_align = ' text-center';
}
switch ( $block_text_align ) {
	case 'left' :
		$block_text_align 	= ' text-left';
		break;
	case 'right' :
		$block_text_align = ' text-right';
		break;
	default :
		$block_text_align = ' ';
}
switch ( $icon_placement ) {
	case 'before' :
		$icon_align 	= ' left';
		break;
	case 'after' :
		$icon_align = ' right';
		break;
	case 'next' :
		$icon_align = ' center';
}


// Check if the icon_enabled has been toggled to be 'true'
if ( '1' == $icon_enabled ){
	$icon_output .= '<i class="pw_cs_icon x-icon-'.$icon.$icon_align.'" data-x-icon="&#x'.$unicode.'" aria-hidden="true"></i>';
} else {
	$icon_output = '';
}

// Output the Shortcode 

	$output_shortcode = '<div class="button-holder'.$button_align.'">';
	$output_shortcode.= '<a href="'.$href.'" class="button custom-button'.$class.$size.$block.$button_bg_colour.$block_text_align.'"'.$target.'>';

	// Position the icon before the main text
	if ( 'before' == $icon_placement ){
		$output_shortcode.= $icon_output;
	}

	$output_shortcode.= $button_text;

	// If necessary, place the icon after the main text
	if ( 'after' == $icon_placement ){
		$output_shortcode.= $icon_output;
	}

	$output_shortcode.= '</a>';
	$output_shortcode.= '</div>';


echo $output_shortcode;

