<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Defaults Values: "Dropdown Links Item"
 */

return array(
	'id'				=> '',
	'class'				=> '',
	'style'				=> '',
	'button_text'		=> '',
	'size'				=> 'regular',
	'button_align'		=> 'left',
	'button_bg_colour'	=> 'primary-blue',
	'block'				=> '',
	'block_text_align'	=> 'left',
	'icon_enabled'		=> '',
	'icon_placement'	=> 'after',
	'icon'				=> '',
	'href'				=> '',
	'title'				=> '',
	'target'			=> '',
);