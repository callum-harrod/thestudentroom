<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Shortcode handler: "CTA Buttons"
 */
?>

<?php

$prefix               = 'pwl_conquip_products_';
$products_product_url = get_post_meta( get_the_ID(), $prefix . 'product_download', true );
$gravity_form_id      = get_post_meta( get_the_ID(), $prefix . 'gravity_form_id', true );

?>

<?php if ( ! empty( $gravity_form_id ) ) : ?>

	<div class="button-wrapper cta-buttons">
		<a class="arrow-button arrow-modal-button" href="#"><?php esc_html_e( 'Request a quote', 'pw-cornerstone-extended' ); ?></a>
		<?php if ( ! empty( $products_product_url ) ) : ?>
			<a class="user-guide" target="_blank" href="<?php echo esc_url( $products_product_url ); ?>"><?php esc_html_e( 'User Guide', 'pw-cornerstone-extended' ); ?></a>
		<?php endif; ?>

	</div>

	<div class="offcanvas-request-quote">
		<a href="#" id="close-offcanvas-request-quote">
			<img src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/images/close.png' ); ?>" class="search-image">
		</a>
		<div class="form-wrapper">
			<h3 class="form-title">
				<?php
					echo esc_html(
						sprintf(
							/* Translators: the name of the product to be requested */
							__( '%s Hire Request', 'pw-cornerstone-extended' ),
							get_the_title()
						)
					);
				?>
			</h3>
			<?php echo do_shortcode( '[gravityform id="' . esc_html( $gravity_form_id ) . '" title="false" description="true" ajax="true"]' ); ?>
		</div>
	</div>

<?php else : ?>

	<div class="button-wrapper cta-buttons">
		<a class="arrow-button arrow-modal-button" href="<?php echo esc_url( site_url( '/contact/' ) ); ?>"><?php esc_html_e( 'Request a quote', 'pw-cornerstone-extended' ); ?></a>
		<?php if ( ! empty( $products_product_url ) ) : ?>
			<a class="user-guide" target="_blank" href="<?php echo esc_url( $products_product_url ); ?>"><?php esc_html_e( 'User Guide', 'pw-cornerstone-extended' ); ?></a>
		<?php endif; ?>

	</div>

<?php endif; ?>
