var $ = jQuery.noConflict();

// Search Toggle
$(document).ready(function(){
    $('.arrow-modal-button').bind('click',function(event){
        $( '.offcanvas-request-quote' ).toggleClass( "active" );
        $( 'arrow-modal-button' ).toggleClass( "active" );
        $( 'body' ).toggleClass( "lock" );
    });

    $('#close-offcanvas-request-quote').bind('click',function(event){
        $( '.offcanvas-request-quote' ).toggleClass( "active" );
        $( 'arrow-modal-button' ).toggleClass( "active" );
        $( 'body' ).toggleClass( "lock" );
    });
});

$(document).keyup(function(e) {
    if (e.keyCode == 27 && $('.offcanvas-request-quote').hasClass( "active" )) {
        $( '.offcanvas-request-quote' ).toggleClass( "active" );
        $( 'arrow-modal-button' ).toggleClass( "active" );
        $( '.tcon' ).toggleClass('tcon-transform');
    }
});