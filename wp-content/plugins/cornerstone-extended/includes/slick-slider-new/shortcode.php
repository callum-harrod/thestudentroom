<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Shortcode handler: "Slick Slider New"
 */

$post_id = get_the_id();
$id      = ( '' != $id ) ? 'id="' . esc_attr( $id ) . '"' : '';
$class   = ( '' != $class ) ? ' ' . esc_attr( $class ) : '';

// Note $style is pre-escaped and checked by Cornerstone.
$style = ( '' != $style ) ? ' style="min-height:' . $slider_height . 'px;' . $style . '"' : ' style="min-height:' . $slider_height . 'px' . '"';

// Indentation - just helps when viewing source, so you can see the Slick Slider jQuery settings properly
$tab  = "\t";
$tab2 = $tab . $tab;
$tab3 = $tab . $tab . $tab;
$tab4 = $tab . $tab . $tab . $tab;
$lbr  = "\n"; // line break for View Source output

$slider_settings = array();

$slider_settings_output = '';

foreach ( $slider_settings as $setting ) {
	$slider_settings_output .= ( '' != $setting ) ? $tab2 . $setting . ',' . $lbr : '';
}

global $_multislider_slide_context;
$_multislider_slide_context = 'top';

$i_arrow_left  = "<i class='fa fa-angle-left slick-prev'></i>";
$i_arrow_right = "<i class='fa fa-angle-right slick-prev'></i>";

// Start outputting the shortcode
$output_shortcode  = '<div ' . $id . ' class="cs_multi_slider_top ' . $class . '"' . $style . '>';
$output_shortcode .= do_shortcode( $content );
$output_shortcode .= '</div>';

$output_shortcode .= '<script type="text/javascript">' . $lbr;
$output_shortcode .= 'jQuery(document).ready(function($){

	s1 = $(".cs_multi_slider_top").slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		prevArrow: "' . $i_arrow_left . '",
		nextArrow: "' . $i_arrow_right . '",
		dots: true,
		fade: true,
		autoplay: true,
		autoplaySpeed: 7000,
		pauseOnHover: true,
		pauseOnFocus: true,
		pauseOnDotsHover: true,
	});

	$(".cs_multi_slider_top").on("beforeChange", function() {
		jQuery("iframe").each(function() {
			this.contentWindow.postMessage(\'{"event":"command","func":"stopVideo","args":""}\', "*");
		});
	});
});' . $lbr;
$output_shortcode .= '</script>' . $lbr;

echo $output_shortcode;
