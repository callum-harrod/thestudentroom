<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Shortcode handler: "FacetWP Filters	"
 */

$post_id 		= get_the_id();
$id				= ( $id    != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
$class			= ( $class != '' ) ? ' ' . esc_attr( $class ) : '';
$style			= ( $style != '' ) ? 'style="' . $style . '"' : ''; // Note $style is pre-escaped and checked by Cornerstone.
$fwp_facet_array		= explode( '|', $fwp_facet );
$fwp_taxonomy_source	= str_replace( 'tax/', '', $fwp_facet_array[0]);
$fwp_facet_output		= $fwp_facet_array[1];
$output_shortcode = '';

/**
 * Check for Cornerstone
 * Due to FacetWP not rendering properly in Cornerstone, the $is_cornerstone variable is set to allow a different display to be provided when using the Cornerstone editor.
 */
parse_str( $_SERVER["QUERY_STRING"], $query_array );
$is_cornerstone 	= ( 'cs_render_element' == $query_array['action'] ) ? true : false;

/**
 * Check if FacetWP is activated first
 */
if ( class_exists( 'FacetWP' ) ){

	/**
	 * FacetWP Facet - filter display above the post content loop
	 * Note: cannot be inside of the template output!
	 */
	$output_shortcode	.= '<div class="pw_cs_facet_filter row'.$filter_class.' '.$button_color.'">';
	$output_shortcode	.= '<div class="pw_cs_facet '.$class.' small-12 label_'.$fwp_label_position.' row">';

	if ( $show_filter_label ){
		$output_shortcode	.= ( 'left' == $fwp_label_position ) ? '<div class="filter_label column medium-1">'.__( $fwp_filter_label, 'pw-cornerstone-extended').'</div>' : '<div class="filter_label column small-12">'.__( $fwp_filter_label, 'pw-cornerstone-extended').'</div>';
	}
	
	
	if ( $is_cornerstone ){ // Alternative display for Cornerstone
		// $output_shortcode 	.= '<div><strong>Please Save the page and view on the front end to see FacetWP Facet filters working</strong></div>';
		
		$taxonomy_terms		= get_terms([
			'taxonomy' 		=> $fwp_taxonomy_source,
		]);

		foreach ($taxonomy_terms as $terms) {
			$term_names[]	.= '<div class="facetwp-radio cornerstone_active">'.$terms->name.'</div>';
		}
		$terms_list = implode( '', $term_names );		
		$output_shortcode	.= ( 'left' == $fwp_label_position ) ? '<div class="column medium-11">'.$terms_list.'</div>' : '<div class="column small-12">'.$terms_list.'</div>' ;
		
	} else {
		// Output the FacetWP Facet for the front end display
		$output_shortcode	.= ( 'left' == $fwp_label_position ) ? '<div class="column medium-11">'.facetwp_display( 'facet', $fwp_facet_output ).'</div>' : '<div class="column small-12">'.facetwp_display( 'facet', $fwp_facet_output ).'</div>';
		

	}

	$facet_reset_class		 = ( 'left' == $fwp_label_position ) ? ' column medium-11 medium-offset-1' : '';
	$output_shortcode		.= ( $show_reset_button ) ? '<div class="facet_reset columns small-12'.$facet_reset_class.'"><a class="button-facetwp view_all" onclick="FWP.reset()">'.__( $reset_button_text, 'pw-cornerstone-extended').'</a></div>' : '';
	
	$output_shortcode		.= '</div>'; // .medium-12
	$output_shortcode		.= '</div>'; // .pw_cs_facet_filter

} else {

	/**
	 * FacetWP Doesn't Exist Message!
	 */
	$output_shortcode .= '<strong style="color:#900;">'.__('Please activate the Facet WP on the Plugins page.', 'pw-cornerstone-extended').'</strong>';

}

// Display the Output
$output .= $output_shortcode; 

echo $output;

