<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Element Controls: "Custom Button"
 */

return array(

	'button_text' => array(
		'type'    => 'text',
		'ui' => array(
			'title' => __( 'Text', 'pw-cornerstone-extended' ),
			'tooltip' =>__( 'Button description.', 'pw-cornerstone-extended' ),
		),
		'suggest' => __( 'Click Me!', 'pw-cornerstone-extended' ),
		'autofocus' => array(
			'content' => '.x-btn',
		)
	),

	'href' => array(
		'type'		=> 'text',
		'ui' => array(
			'title' => __( 'href', 'pw-cornerstone-extended' ),
			'tooltip' =>__( 'Enter in the URL you want to link to.', 'pw-cornerstone-extended' ),
		),
		'suggest' => __( '#', 'pw-cornerstone-extended' ),
	),

	'title' => array(
		'type'		=> 'text',
		'ui' => array(
			'title' => __( 'Link Title Attribute', 'pw-cornerstone-extended' ),
			'tooltip' =>__( 'Enter in the title attribute you want for your link.', 'pw-cornerstone-extended' ),
		),
		'suggest' => __( '', 'pw-cornerstone-extended' ),
	),
	'target' => array(
		'type'    => 'toggle',
		'ui' => array(
			'title'   => __( 'Open Link in New Window', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Select to open your link in a new window.', 'pw-cornerstone-extended' ),
		),
	),

	'size' => array(
		'type' => 'select',
		'ui' => array(
			'title'   => __( 'Size', 'pw-cornerstone-extended' ),
			'tooltip' =>__( 'Select the button size.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
		'suggest' => 'regular',
		'options' => array(
			'choices' => array(
				array( 'value' => 'small',   'label' => __( 'Small', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'regular', 'label' => __( 'Normal', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'large',  'label' => __( 'Large', 'pw-cornerstone-extended' ) ),
			),
		),
	),

	'button_align' => array(
		'type' 	=> 'choose',
		'context'	=> 'content',
		'ui' => array(
			'title' => __( 'Button Alignment', 'pw-cornerstone-extended' ),
			'tooltip' =>__( 'The alignment of the button in relation to it\'s container.', 'pw-cornerstone-extended' ),
		),
		'suggest' => 'left',
		'options' => array(
			'columns' => '3',
			'choices' => array(
				array( 'value' => 'left',   'tooltip' => __( 'Align Left', 'pw-cornerstone-extended' ),   'icon' => fa_entity( 'align-left' ) ),
				array( 'value' => 'center', 'tooltip' => __( 'Align Center', 'pw-cornerstone-extended' ), 'icon' => fa_entity( 'align-center' ) ),
				array( 'value' => 'right', 'tooltip' => __( 'Align Right', 'pw-cornerstone-extended' ), 'icon' => fa_entity( 'align-right' ) ),
			)
		),
	),

	'button_bg_colour' => array(
		// Use 'condition' to control displaying this section, depending on whether 'icon_enabled' has been toggled on or off.
		// 'condition' => array(
		// 	'icon_enabled' => true,
		// ),

		'type' => 'select',
		'ui' => array(
			'title'   => __( 'Button Background Color', 'pw-cornerstone-extended' ),
			'tooltip' =>__( 'Choose a custom background color for your Button.', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
		'suggest' => 'primary-blue',
		'options' => array(
			'choices' => array(
				array( 'value' => 'primary-blue',   'label' => __( 'Primary Blue', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'secondary-blue', 'label' => __( 'Secondary Mid-Blue', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'primary-green',  'label' => __( 'Primary Green', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'secondary-green',  'label' => __( 'Secondary Mid-Green', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'primary-turquoise',  'label' => __( 'Primary Turquoise', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'secondary-purple',  'label' => __( 'Secondary Purple', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'light-gray',  'label' => __( 'Secondary Light Grey', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'dark-gray',  'label' => __( 'Secondary Dark Grey', 'pw-cornerstone-extended' ) ),
			),
		),
	),

	'block' => array(
		'type'    => 'toggle',
		'ui' => array(
			'title'   => __( 'Block', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Select to make your button go fullwidth.', 'pw-cornerstone-extended' ),
		),
	),

	'block_text_align' => array(
		// Use 'condition' to control displaying this section, depending on whether 'icon_enabled' has been toggled on or off.
		'condition' => array(
			'block' => true,
		),
		'type' 	=> 'choose',
		'context'	=> 'content',
		'ui' => array(
			'title' => __( 'Text Alignment', 'pw-cornerstone-extended' ),
			'tooltip' =>__( 'The alignment of the text inside the expanded button.', 'pw-cornerstone-extended' ),
		),
		'suggest' => 'left',
		'options' => array(
			'columns' => '3',
			'choices' => array(
				array( 'value' => 'left',   'tooltip' => __( 'Align Left', 'pw-cornerstone-extended' ),   'icon' => fa_entity( 'align-left' ) ),
				array( 'value' => 'center', 'tooltip' => __( 'Align Center', 'pw-cornerstone-extended' ), 'icon' => fa_entity( 'align-center' ) ),
				array( 'value' => 'right', 'tooltip' => __( 'Align Right', 'pw-cornerstone-extended' ), 'icon' => fa_entity( 'align-right' ) ),
			)
		),
	),

	'icon_enabled' => array(
		'type'    => 'toggle',
		'ui' => array(
			'title'   => __( 'Use Icon?', 'pw-cornerstone-extended' ),
			'tooltip' => __( 'Select to make your button go fullwidth.', 'pw-cornerstone-extended' ),
		),
		'suggest' => '',
	),

	'icon_placement' => array(
		// Use 'condition' to control displaying this section, depending on whether 'icon_enabled' has been toggled on or off.
		'condition' => array(
			'icon_enabled' => true,
		),
		'type' => 'select',
		'ui' => array(
			'title'   => __( 'Icon Placement', 'pw-cornerstone-extended' ),
			'tooltip' =>__( 'Select whether the icon should appear before or after the content', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
		'suggest' => 'after',
		'options' => array(
			'choices' => array(
				array( 'value' => 'before',   'label' => __( 'Before Text', 'pw-cornerstone-extended' ) ),
				array( 'value' => 'after', 'label' => __( 'After Text', 'pw-cornerstone-extended' ) ),
			),
		),
	),

	// Icon Chooser section
	'icon' => array(
		// Use 'condition' to control displaying this section, depending on whether 'icon_enabled' has been toggled on or off.
		'condition' => array(
			'icon_enabled' => true,
		),

		'type' => 'icon-choose',
		'ui' => array(
			'title' => __( 'Icon', 'pw-cornerstone-extended' ),
		),
		'context' => 'content',
		'suggest' => 'lightbulb-o',
	),
);