<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Shortcode handler: "FacetWP Posts"
 */

define( 'PWL_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

$post_id 			= get_the_id();
$id					= ( $id    != '' ) ? 'id="' . esc_attr( $id ) . '"' : '';
$class				= ( $class != '' ) ? ' ' . esc_attr( $class ) : '';
$style				= ( $style != '' ) ? 'style="' . $style . '"' : ''; // Note $style is pre-escaped and checked by Cornerstone.

$output_shortcode 	= '';

/**
 * Check if FacetWP is activated first
 */
if ( class_exists( 'FacetWP' ) ){


	// Calculate the column sizes for Foundation, and pass this to WP Query
	switch ( $posts_per_row ) {
		case '2':
			$column_size = 6;
			$number_of_posts = $max_posts_2;
			break;
		case '3':
			$column_size = 4;
			$number_of_posts = $max_posts_3;
			break;
		case '4':
			$column_size = 3;
			$number_of_posts = $max_posts_4;
			break;
	}

	// WP_Query arguments
	$facetwp_args = array (
		'post_type'              => array( $post_type ),
		'post_status'            => array( 'publish' ),
		'posts_per_page'         => $number_of_posts,
		'order'                  => 'DESC',
		'orderby'                => 'date',
		'ignore_sticky_posts'    => true,
		'facetwp'				 => true,
	);

	// The Query
	$facetwp_query = new WP_Query( $facetwp_args );
	$facetwp_posts_output = ''; // String for FacetWP content output
	$facetwp_post_count = 1; // Used to determine when the row ends

	// The Loop
	if ( $facetwp_query->have_posts() ) {
		while ($facetwp_query -> have_posts()) : $facetwp_query -> the_post();

			$article_id			= get_the_ID();
			$article_type		= get_post_type( $article_id );

			// Add Rows if necessary
			if ( 1 == $facetwp_post_count % $posts_per_row ){
				$facetwp_posts_output 	.= '<div class="row x-container max">';
			}

			$start_class = ( 1 == $facetwp_post_count % $posts_per_row ) ? ' start' : '';
			$end_class = ( ($facetwp_post_count == $facetwp_query->post_count) || ( 0 == $facetwp_post_count % $posts_per_row ) ) ? ' end' : ''; // Add the 'end' class

			// Article Content
			$facetwp_posts_output 		.= '<div class="facetwp_excerpt x-column x-1-'.$posts_per_row.$start_class.$end_class.'">';// Column container - START
			$facetwp_posts_output 		.= '<a href="'.get_the_permalink().'">';
			// $facetwp_posts_output 		.= ( $show_border ) ? '<div class="border">': '';
			
			// Display Featured Image Above Title
			// $facetwp_posts_output	.= ( 'above_title' == $featured_image ) ? '<div class="featured_image above_title">'.pw_cse_display_post_thumbnail( $article_id, $size = 'medium' ).'</div>' : '';

			if ( has_post_thumbnail() ){
				$facetwp_posts_output 	.= get_the_post_thumbnail( $page->ID, 'large' );
			} else {
				$facetwp_posts_output 	.= '<img class="logo" src="'.PWL_PLUGIN_URL . 'assets/images/logo.png">';
			}
			

			// Display the Title
			$facetwp_posts_output 		.= '<h3 class="facetwp_title"><span class="fwp_title_text">'.get_the_title().'</span></h3>'; // Use 'true' to "echo" output, and 'false' to "return" output

			// $facetwp_posts_output 		.= ( $show_border ) ? '</div>': '';
			$facetwp_posts_output		.= '</a>';
			$facetwp_posts_output 		.= '</div>'."\n";// Column container - END

			// Add Rows if necessary
			if ( ( 0 == $facetwp_post_count % $posts_per_row ) || ($facetwp_post_count == $facetwp_query->post_count) ){
				$facetwp_posts_output 	.= '</div>';
			}

			$facetwp_post_count ++;


		endwhile;
	} else {	
		// No posts
		$output_shortcode .= '<strong style="color:#900;">'.__('No '.$post_type.' Posts Found.', 'pw-cornerstone-extended').'</strong>';
	}

	// Restore original Post Data
	wp_reset_postdata();

	$output_shortcode 	.= '<div '.$id.' class="facetwp-template '.$class.'">'; // Use 'true' to "echo" output, and 'false' to "return" output
	$output_shortcode	.= $facetwp_posts_output;
	$output_shortcode 	.= '</div>';

	/**
	 * FacetWP - Pagination
	 */

	/**
	 * Show/Hide FacetWP Pagination
	 */
	if ( $show_pager ){
		$output_shortcode	.= '<div class="pw_cs_facetwp_pager '.$pager_position.'">';

		// if ( $is_cornerstone ){ // Alternative display for Cornerstone - this is an illusion
		// 	for ($i=1; $i <= $facetwp_query->max_num_pages; $i++) { 
		// 		$active_class = ( 1 == $i ) ? ' active' : '';
		// 		$output_shortcode .= '<a href="#" class="facetwp-page button custom-button button-facetwp cornerstone_active '.$active_class.'">'.($i).'</a>';
		// 	}
		// } else {
			$output_shortcode .= facetwp_display( 'pager' );
		// }
		
		$output_shortcode	.= '</div>';
	}

} else {

	/**
	 * FacetWP Doesn't Exist Message!
	 */
	$output_shortcode .= '<strong style="color:#900;">'.__('Please activate the Facet WP on the Plugins page.', 'pw-cornerstone-extended').'</strong>';

}


// Display the Output
$output .= '<div class="pw_cs_facetwp row '.$button_color.'">';
$output .= $output_shortcode; 
$output .= '</div>';
echo $output;