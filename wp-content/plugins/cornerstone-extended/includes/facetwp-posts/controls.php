<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Element Controls: "FacetWP Posts"
 */


/**
 * Generate a list of all Post Types
 */

$post_types_args = array(
   'public'   			=> true,
   'show_ui' 			=> true,
);
$post_types_array = array();

$post_types_list = get_post_types( $post_types_args, 'objects' );
unset($post_types_list['attachment']); // Remove Attachments
unset($post_types_list['pages']); // Remove Pages... because there are no Categories to filter with Pages

foreach ($post_types_list as $post_type ) {
	$post_types_array[] = array( 'value' => $post_type->name, 'label'	=> $post_type->label, 'pw-cornerstone-extended' );
}

/**
 * Generate a list of all Theme Taxonomy Terms
 */

// $taxonomy_terms		= get_terms([
// 	'taxonomy' 		=> array('alias_tags','post_tag'),
// ]);
// $terms_array 	= array();
// $terms_array[]	=  array( 'value' => '', 'label'	=> 'Select', 'pw-cornerstone-extended' );

// foreach ($taxonomy_terms as $terms) {
// 	$terms_array[]	=  array( 'value' => $terms->slug, 'label'	=> $terms->name, 'pw-cornerstone-extended' );
// }

return array(

	'post_type'	=> array(
		'type' 		=> 'select',
		'ui' 		=> array(
			'title'		=> __( 'Post Type', 'pw-cornerstone-extended' ),
			'tooltip'	=>__( 'Select the Post Type for the display.', 'pw-cornerstone-extended' ),
		),
		'context'	=> 'content',
		'suggest'	=> 'post', // default choice selected from below
		'options'	=> array(
			'choices'	=> $post_types_array,
		),
	),

	'posts_per_row'	=> array(
		'type' 		=> 'select',
		'ui' 		=> array(
			'title'		=> __( 'Number of Posts Per Row', 'pw-cornerstone-extended' ),
			'tooltip'	=>__( 'Select the number of Posts to display on each row.', 'pw-cornerstone-extended' ),
		),
		'context'	=> 'content',
		'suggest'	=> '4', // default choice selected from below
		'options'	=> array(
			'choices'	=> array(
				array( 'value' => '2',	'label'	=> __( '2 per row', 'pw-cornerstone-extended' ) ),
				array( 'value' => '3',	'label'	=> __( '3 per row', 'pw-cornerstone-extended' ) ),
				array( 'value' => '4',	'label'	=> __( '4 per row', 'pw-cornerstone-extended' ) ),
			),
		),
	),

	// Options to display if "2 per row" is selected for 'posts_per_row'
	'max_posts_2'	=> array(
		'type' 		=> 'select',
		'ui' 		=> array(
			'title'		=> __( 'Number of Posts Per Page', 'pw-cornerstone-extended' ),
			'tooltip'	=>__( 'Select the maximum number of posts to be displayed.', 'pw-cornerstone-extended' ),
		),
		'context'	=> 'content',
		'suggest'	=> '6', // default choice selected from below
		'options'	=> array(
			'choices'	=> array(
				array( 'value' => '2',	'label'	=> __( '2', 'pw-cornerstone-extended' ) ),
				array( 'value' => '4',	'label'	=> __( '4', 'pw-cornerstone-extended' ) ),
				array( 'value' => '6',	'label'	=> __( '6', 'pw-cornerstone-extended' ) ),
				array( 'value' => '8',	'label'	=> __( '8', 'pw-cornerstone-extended' ) ),
				array( 'value' => '10',	'label'	=> __( '10', 'pw-cornerstone-extended' ) ),
				array( 'value' => '12',	'label'	=> __( '12', 'pw-cornerstone-extended' ) ),
			),
		),
		'condition' => array(
			'posts_per_row' => '2',
		),
	),

	// Options to display if "3 per row" is selected for 'posts_per_row'
	'max_posts_3'	=> array(
		'type' 		=> 'select',
		'ui' 		=> array(
			'title'		=> __( 'Maximum Number of Posts', 'pw-cornerstone-extended' ),
			'tooltip'	=>__( 'Select the maximum number of posts to be displayed.', 'pw-cornerstone-extended' ),
		),
		'context'	=> 'content',
		'suggest'	=> '6', // default choice selected from below
		'options'	=> array(
			'choices'	=> array(
				array( 'value' => '3',	'label'	=> __( '3', 'pw-cornerstone-extended' ) ),
				array( 'value' => '6',	'label'	=> __( '6', 'pw-cornerstone-extended' ) ),
				array( 'value' => '9',	'label'	=> __( '9', 'pw-cornerstone-extended' ) ),
				array( 'value' => '12',	'label'	=> __( '12', 'pw-cornerstone-extended' ) ),
			),
		),
		'condition' => array(
			'posts_per_row' => '3',
		),
	),

	// Options to display if "3 per row" is selected for 'posts_per_row'
	'max_posts_4'	=> array(
		'type' 		=> 'select',
		'ui' 		=> array(
			'title'		=> __( 'Maximum Number of Posts', 'pw-cornerstone-extended' ),
			'tooltip'	=>__( 'Select the maximum number of posts to be displayed.', 'pw-cornerstone-extended' ),
		),
		'context'	=> 'content',
		'suggest'	=> '8', // default choice selected from below
		'options'	=> array(
			'choices'	=> array(
				array( 'value' => '4',	'label'	=> __( '4', 'pw-cornerstone-extended' ) ),
				array( 'value' => '8',	'label'	=> __( '8', 'pw-cornerstone-extended' ) ),
				array( 'value' => '12',	'label'	=> __( '12', 'pw-cornerstone-extended' ) ),
			),
		),
		'condition' => array(
			'posts_per_row' => '4',
		),
	),

	'show_pager'	=> array(
		'type'			=> 'toggle',
		'context'		=> 'content',
		'suggest'		=> '1',
		'ui'			=> array(
			'title'			=> __( 'Show Pager', 'pw-cornerstone-extended' ),
			'tooltip'		=>__( 'Show or hide the Post Type pagination numbers.', 'pw-cornerstone-extended' ),
		),
	),

	'pager_position' => array(
		'type'		=> 'choose',
		'context'	=> 'content',
		'ui' => array(
			'title'			=> __( 'Pager Position', 'pw-cornerstone-extended' ),
			'tooltip'		=> __( 'Position the Pager left, center or right.', 'pw-cornerstone-extended' ),
		),
		'suggest'	=> 'text-left',
		'options'	=> array(
			'columns'	=> '3',
			'choices'	=> array(
				array( 'value' => 'text-left',		'tooltip' => __( 'Align Left', 'pw-cornerstone-extended' ),   'icon' => fa_entity( 'align-left' ) ),
				array( 'value' => 'text-center',	'tooltip' => __( 'Align Center', 'pw-cornerstone-extended' ), 'icon' => fa_entity( 'align-center' ) ),
				array( 'value' => 'text-right',		'tooltip' => __( 'Align Right', 'pw-cornerstone-extended' ), 'icon' => fa_entity( 'align-right' ) ),
			)
		),
		'condition' => array(
			'show_pager' => true,
		),
	),

);
