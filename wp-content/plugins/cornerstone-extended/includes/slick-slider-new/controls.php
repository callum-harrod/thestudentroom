<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Element Controls: "Slick Slider New"
 */

// Extra Colours
// global $brand_colors_extra; // Set in the cornerstone-extended.php file (to control all brand background colour options for a select)

return array(

	// Note: for the 'floor' and 'capacity' to work, this control *MUST* be called "elements".
	// Changing this value will only show the "Add" button, but not the default number set by the upper and lower limits
	'elements' => array(
		'type'    => 'sortable',
		'options' => array(
			'element' => 'slick-slider-slide-item-new',
			'newTitle' => __( 'Slide %s', 'pw-cornerstone-extended' ),
			'floor'   => 1, // Slide Lower Limit - minimum number of Slides to show
			'capacity' => 15, // Slide Upper Limit - maximum number of Slides to show
			'title_field' => 'heading'
		),
		'context' => 'content',
		'suggest' => array(
			array( 'heading' => __( 'Slide 1', 'pw-cornerstone-extended' ) ),
			array( 'heading' => __( 'Slide 2', 'pw-cornerstone-extended' ) ),
		),
	),
);