var $ = jQuery.noConflict();

// Read More Functionality
$(document).ready(function(){

    // On Tab Button Click
    $('.tab-activator button').bind('click touchstart',function(event){

        // Get Tab ID from tab button
        var tabID = $(this).data('tab-id');

        // Remove all active classes from tabs
        $( ".tab" ).each(function( index ) {
            $( this ).removeClass( "active" );
        });

        // Remove all active classes from tabs
        $( ".tab-activator button" ).each(function( index ) {
            $( this ).removeClass( "active" );
        });

        // Activate this Tab only
        $( '#' +  tabID).toggleClass( "active" );
        $( this ).toggleClass( "active" );
    });
});