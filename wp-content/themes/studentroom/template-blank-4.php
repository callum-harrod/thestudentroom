<?php
/*
Template Name: Blank - Student Room No Container | Header, Footer
Template Post Type: post, page, products
*/
// =============================================================================
//
// A blank page for creating unique layouts.
//
// Content is output based on which Stack has been selected in the Customizer.
// To view and/or edit the markup of your Stack's index, first go to "views"
// inside the "framework" subdirectory. Once inside, find your Stack's folder
// and look for a file called "template-blank-4.php," where you'll be able to
// find the appropriate output.
// =============================================================================
//get_view( x_get_stack(), 'template', 'blank-4' );
?>



<?php get_header(); ?>

  <div class="x-main full" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php x_get_view( 'global', '_content', 'the-content' ); ?>
      </article>

    <?php endwhile; ?>


    <?php
		if ( function_exists('yoast_breadcrumb') ) {
		  yoast_breadcrumb( '</p><p id="breadcrumbs">','</p><p>' );
		}
	?>

  </div>

<?php get_footer(); ?>