<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Defaults Values: "Slick Slider New"
 */

return array(

	'id'			=> '',
	'class'			=> '',
	'style'			=> '',
	'elements'		=> '',
	'slider_height'	=> '',
	'slider_initial_slide'	=> '',
	
	'slider_fade'	=> '',
	'slider_num_items'	=> '',

	'slider_arrows'	=> '',

	'slider_dots'	=> '',
	'use_dots_color'	=> '',
	'slider_dots_color_brand'	=> '',
	'slider_dots_color_custom'	=> '',
	'slider_dots_align'	=> '',
	'slider_dots_valign'	=> '',

	'slider_autoplay'	=> '',
	'slider_autoplay_speed'	=> '',
	'slider_transition_speed'	=> '',
	'slider_loop'	=> '',
	
	
);

