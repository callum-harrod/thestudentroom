<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Defaults Values: "Slick Slider Slide Item New"
 */

return array(

	'id'              => '',
	'class'           => '',
	'style'           => '',

	'use_text'        => '',
	'slide_text'      => '',

	'slide_bg_type'   => '',
	'slide_bg_image'  => '',
	'slide_bg_video'  => '',
	'slide_content'   => '',
	'slide_title'     => '',
	'slide_text'      => '',
	'slide_cta_title' => '',
	'slide_cta_url'   => '',

	// Set by the definition.php file. Required in this file, for this attribute to work between the Parent and Child elements.
	'slider_height'   => '',

);
