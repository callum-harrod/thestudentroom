<?php defined( 'ABSPATH' ) || die( 'Direct file access is forbidden.' );

/**
 * Defaults Values: "FacetWP Posts"
 */

return array(

	'id'				=> '',
	'class'				=> '',
	'style'				=> '',
	'post_type'			=> '',
	'taxonomy_name'		=> '',
	'posts_per_row'		=> '',
	'max_posts_2'		=> '4',
	'max_posts_3'		=> '6',
	'max_posts_4'		=> '8',
	'show_pager'		=> '',
	'pager_position'	=> '',
	
	
);

