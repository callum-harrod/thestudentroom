<?php

/**
 * Shortcode handler
 */

	$_content .= '<div class="x-search-shortcode cornerstone-archive-search">';
	$_content .= '<form method="get" id="searchform" class="form-search">';
	$_content .= '<label for="archive_query">Search products</label>';
	$_content .= '<div class="search-query-container"><input type="text" id="archive_query" class="search-query" name="archive_query"></div>';
	$_content .= '</form></div>';

	$_paged_query_var = $_GET['archive_page'];

	if( ! $_paged_query_var || ! is_numeric( $_paged_query_var ) ) :
		$_paged_query_var = 1;
	endif;

	$_search_query_var = $_GET['archive_query'];

	if( ! $_search_query_var ) :

		$_search_query = '';

		$_args = array(
			'post_type'			=> $post_type,
			'posts_per_page'	=> $count,
			'paged'				=> $_paged_query_var,
			'order'				=> 'ASC',
			'orderby'			=> 'menu_order title',
			'meta_query' => array(
				array(
					'key'     => '_thumbnail_id',
				),
			),
		);

		$_resources = new WP_Query( $_args );

	else :

		$_search_query = esc_attr( $_search_query_var );

		$_args = array(
			'post_type'			=> $post_type,
			'posts_per_page'	=> $count,
			'page'				=> $_paged_query_var,
			's'					=> $_search_query,
			'meta_query' => array(
				array(
					'key'     => '_thumbnail_id',
				),
			),
		);

		$_search_resources = new SWP_Query( $_args );

	endif;


	if( ( $_resources && $_resources->have_posts() ) || ( $_search_resources && ( ! empty( $_search_resources->posts ) ) ) ) :

		$i = 0;

		
		$_content .= '<div class="x-container">';

		if( ! empty( $_search_query ) ) :

			$_content .= '<h2 class="h5">Showing search results for: ' . $_search_query . '</h2>';

		endif;

		if( $_resources ) :
			$_number_of_pages = $_resources->max_num_pages;
		elseif( $_search_resources ) :
			$_number_of_pages = $_search_resources->max_num_pages;
		endif;

		if( $_number_of_pages ) :
			// Generate pagination, to fulfil $current_page, $total_pages, and $search_query.
			$_pagination = cornerstone_archive_pagination( $_paged_query_var, $_number_of_pages, $_search_query );
		else :
			$_pagination = false;
		endif;

		// If false, don't show.
		if( false !== $_pagination ) :

			$_content .= $_pagination;

		endif;

		if( $_resources ) :

			while ( $_resources->have_posts() ) :

				$i++;

				$_resources->the_post();

				$_featured_image_url = get_the_post_thumbnail_url( null, 'full' );

				$_content .= '<div class="x-column x-sm x-1-' . $columns . ' archive-widget">';
				$_content .= '<a href="' . get_the_permalink() . '" title="Read more about our ' . get_the_title() .'">';
				$_content .= '<img src="' . $_featured_image_url . '" alt="' . get_the_title() . '" />';
				$_content .= '<h3 class="archive-item">' . get_the_title() . '</h3>';
				$_content .= '</a>';
				$_content .= '</div>';

				if ( $i === $columns ) {
					$i = 0;
					$_content .= '</div>';
					$_content .= '<div class="x-container">';
				}

			endwhile;

		elseif( $_search_resources ) :

			foreach( $_search_resources->posts as $post ) : setup_postdata( $post );

				$i++;

				$_featured_image_url = get_the_post_thumbnail_url( $post->ID, 'full' );

				$_content .= '<div class="x-column x-sm x-1-' . $columns . ' archive-widget">';
				$_content .= '<a href="' . get_the_permalink( $post->ID ) . '" title="Read more about our ' . get_the_title( $post->ID ) .'">';
				$_content .= '<img src="' . $_featured_image_url . '" alt="' . get_the_title( $post->ID ) . '" />';
				$_content .= '<h3 class="archive-item">' . get_the_title( $post->ID ) . '</h3>';
				$_content .= '</a>';
				$_content .= '</div>';

				if ( $i === $columns ) {
					$i = 0;
					$_content .= '</div>';
					$_content .= '<div class="x-container">';
				}

			endforeach; wp_reset_postdata();

		endif;

		$_content .= '</div>';

	
		// Generate pagination, to fulfil $current_page, $total_pages, and $search_query.
		// $_pagination = cornerstone_archive_pagination( $_paged_query_var, $_number_of_pages, $_search_query );

		// If false, don't show.
		if( false !== $_pagination ) :

			$_content .= $_pagination;

		endif;

	else :

		$_content .= '<div class="x-container">';

		$_content .= '<div class="x-column x-sm x-1-1">';

		if( ! empty( $_search_query ) ) :

			$_content .= '<p style="text-align:center;">Sorry, we weren\'t able to find any content under the term: ' . $_search_query . '</p>';
			$_content .= '<p style="text-align:center;">Please contact us for more informationm, or reset your search terms and try looking through some of our other products.</p>';

			$_content .= cornerstone_archive_pagination( 0, 0, $_search_query );

		else :

			$_content .= '<p style="text-align:center;">Sorry, we weren\'t able to find any content. Please contact us for more information.</p>';

		endif;

		$_content .= '</div>';

		$_content .= '</div>';

	endif;

	$_output = $_content;

	echo $_output;